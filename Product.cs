﻿using System.Diagnostics.CodeAnalysis;

namespace MioApp
{
    public class Product
    {
        public string id { get; set; }
        public Campaign? campaign { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string productImage { get; set; }
        public double price { get; set; }

    }

    public class Campaign
    {
        public string name { get; set; }
        public int discountPercent { get; set; }
    }
}
