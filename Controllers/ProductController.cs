﻿using Microsoft.AspNetCore.Mvc;
using MioApp.ExceptionClasses;
using Newtonsoft.Json;

namespace MioApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
       
        [HttpGet("All")]
        public List<Product> Get()
        {
            string dataAsText = System.IO.File.ReadAllText(@".\data.json");
            var products = JsonConvert.DeserializeObject<List<Product>>(dataAsText);
            
            return products;
        }
        [HttpGet("{id}")]
        public Product Get(string id)
        {
            List<Product> products = Get();
            foreach(Product product in products)
            {
                if (id == product.id)
                    return product;
            }
            throw new CompilingException("Could not find product with specified Id");
        }
        [HttpPost("Create")]
        public Product Post(Product product)
        {
            if (product.price <= 0)
                throw new ProductException("Price has to more than 0!");
            if (product.name.Length > 30)
                throw new ProductException("Name has to be fewer than 30 characters!");
            List<Product> products = Get();
            products.Add(product);
            var newConvert = JsonConvert.SerializeObject(products);
            System.IO.File.WriteAllText(@".\data.json", newConvert);
            return product;

        }
        [HttpPatch("{id}/")]
        public Product Patch(string id, Product product)
        {          
            try
            {
                if (product.price <= 0)
                    throw new ProductException("Price has to more than 0!");
                if (product.name.Length > 30)
                    throw new ProductException("Name has to be fewer than 30 characters!");
                List<Product> products = Get();
                foreach(Product prod in products)
                {
                    if (prod.id == id)
                    {
                        prod.name = product.name;
                        prod.description = product.description;
                        prod.price = product.price;
                        prod.productImage = product.productImage;
                        prod.campaign = product.campaign;
                        var newConvert = JsonConvert.SerializeObject(products);
                        System.IO.File.WriteAllText(@".\data.json", newConvert);

                        return prod;
                    }
                }
               

            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
            }

            throw new CompilingException("Cannot update not already existing products!");
        }
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            List<Product> products = Get();         
            products.Remove(Get(id));
            var newConvert = JsonConvert.SerializeObject(products);
            System.IO.File.WriteAllText(@".\data.json", newConvert);
            
            throw new CompilingException("Cannot delete a non existent product!");
        }
    }
}
