﻿namespace MioApp.ExceptionClasses
{
    public class CompilingException : Exception
    {
        public CompilingException(string message) : base(message) { }
    }
}
